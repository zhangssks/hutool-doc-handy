---
pageComponent:
  name: Catalogue
  data:
    path: 01.指南/09.数据库（Hutool-db）
    imgUrl: /img/logo_small.jpg
    description: Hutool-db是一个在JDBC基础上封装的数据库操作工具类，通过包装，使用ActiveRecord思想操作数据库。
title: 数据库（Hutool-db）
date: 2023-04-04 23:00:00
permalink: /module/db/
article: false
comment: false
editLink: false
author:
  name: Hutool
  link: https://hutool.cn/
---