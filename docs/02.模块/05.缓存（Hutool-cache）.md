---
pageComponent:
  name: Catalogue
  data:
    path: 01.指南/05.缓存（Hutool-cache）
    imgUrl: /img/logo_small.jpg
    description: 提供简易的缓存实现，此模块参考了jodd工具中的Cache模块
title: 缓存（Hutool-cache）
date: 2023-04-04 23:00:00
permalink: /module/cache/
article: false
comment: false
editLink: false
author:
  name: Hutool
  link: https://hutool.cn/
---